package com.example.repositories;
import java.util.List;

import com.example.domains.Prodotto;
import com.example.dto.IdProdottoDTO;
import com.example.dto.ProdottoDTO;
import com.example.dto.ProdottoValoreDTO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ProdottoRepository extends JpaRepository<Prodotto, Long> {
 
    @Query("select p from prodotti p where p.giacenza <= p.scorta_minima")
    List<Prodotto> findAllProdottiSottoScorta();

    //create view qry_prodotti_sotto_scorta as select * FROM dbProdotti.prodotti where giacenza<=scorta_minima;

    @Query(nativeQuery = true, value = "select * from qry_prodotti_sotto_scorta;")
    List<Prodotto> findAllProdottiSottoScorta_fromView();

    @Query(nativeQuery = true, value = "select * from prodotti where um=?1 and giacenza >=?2;")
    List<Prodotto> findAllProdottiStessaUM(String um, double giacenza);

 
    @Query(nativeQuery = true, value = "select id,descrizione,um,giacenza,prezzo_unitario,scorta_minima from prodotti")
    List<ProdottoDTO> findAllProdottiView();
    
    @Query(nativeQuery = true, value = "select id from prodotti where giacenza >=?;")
    List<IdProdottoDTO> findByIdProdottoView(double giacenza);

    @Query(nativeQuery = true, value = "select id,(giacenza*prezzo_unitario) as valore from prodotti;")
    List<ProdottoValoreDTO> findAllProdottiValore();

}
