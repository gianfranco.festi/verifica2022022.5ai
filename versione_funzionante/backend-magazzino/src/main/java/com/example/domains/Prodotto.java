package com.example.domains;

import java.math.BigDecimal;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name = "prodotti") // se si specifica solo @Entity con name vale anche per table in db
@Table(name ="prodotti")
public class Prodotto {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    @Column(name="descrizione",nullable = false) 
    String descrizione;

    @Column(name="um") //unità di misura
    String um;
    @Column(name="giacenza")
    double giacenza; // esprime la quantità presente a magazzino

    @Column(name="scorta_minima")
    double scorta_minima; // esprime la quantità presente a magazzino


    @Column(name="prezzo_unitario",precision=19,scale = 4)
    BigDecimal  prezzo_unitario;


    public Prodotto() {
    }

    public Prodotto(Long id, String descrizione, String um, double giacenza, double scorta_minima, BigDecimal prezzo_unitario) {
        this.id = id;
        this.descrizione = descrizione;
        this.um = um;
        this.giacenza = giacenza;
        this.scorta_minima = scorta_minima;
        this.prezzo_unitario = prezzo_unitario;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescrizione() {
        return this.descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public String getUm() {
        return this.um;
    }

    public void setUm(String um) {
        this.um = um;
    }

    public double getGiacenza() {
        return this.giacenza;
    }

    public void setGiacenza(double giacenza) {
        this.giacenza = giacenza;
    }

    public double getScorta_minima() {
        return this.scorta_minima;
    }

    public void setScorta_minima(double scorta_minima) {
        this.scorta_minima = scorta_minima;
    }

    public BigDecimal getPrezzo_unitario() {
        return this.prezzo_unitario;
    }

    public void setPrezzo_unitario(BigDecimal prezzo_unitario) {
        this.prezzo_unitario = prezzo_unitario;
    }

    public Prodotto id(Long id) {
        setId(id);
        return this;
    }

    public Prodotto descrizione(String descrizione) {
        setDescrizione(descrizione);
        return this;
    }

    public Prodotto um(String um) {
        setUm(um);
        return this;
    }

    public Prodotto giacenza(double giacenza) {
        setGiacenza(giacenza);
        return this;
    }

    public Prodotto scorta_minima(double scorta_minima) {
        setScorta_minima(scorta_minima);
        return this;
    }

    public Prodotto prezzo_unitario(BigDecimal prezzo_unitario) {
        setPrezzo_unitario(prezzo_unitario);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Prodotto)) {
            return false;
        }
        Prodotto prodotto = (Prodotto) o;
        return Objects.equals(id, prodotto.id) && Objects.equals(descrizione, prodotto.descrizione) && Objects.equals(um, prodotto.um) && giacenza == prodotto.giacenza && scorta_minima == prodotto.scorta_minima && Objects.equals(prezzo_unitario, prodotto.prezzo_unitario);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, descrizione, um, giacenza, scorta_minima, prezzo_unitario);
    }

    @Override
    public String toString() {
        return "{" +
            " id='" + getId() + "'" +
            ", descrizione='" + getDescrizione() + "'" +
            ", um='" + getUm() + "'" +
            ", giacenza='" + getGiacenza() + "'" +
            ", scorta_minima='" + getScorta_minima() + "'" +
            ", prezzo_unitario='" + getPrezzo_unitario() + "'" +
            "}";
    }

}
