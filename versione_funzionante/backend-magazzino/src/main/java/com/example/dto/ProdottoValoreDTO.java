package com.example.dto;

import java.math.BigDecimal;

public interface ProdottoValoreDTO {
    Long getId();
    BigDecimal getValore();
}
