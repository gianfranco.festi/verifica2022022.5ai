package com.example.dto;

public interface ProdottoDTO {
    Long getId();
    String getDescrizione();
    String getUm();
    double getGiacenza();
    double getScorta_minima(); 
}
