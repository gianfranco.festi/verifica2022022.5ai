package com.example.controllers;

import java.util.List;

import com.example.domains.Prodotto;
import com.example.dto.IdProdottoDTO;
import com.example.dto.ProdottoDTO;
import com.example.dto.ProdottoValoreDTO;
import com.example.repositories.ProdottoRepository;
import com.example.services.ProdottoService;

import io.swagger.v3.oas.annotations.Operation;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path="/api/prodotti", produces = "application/json")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT}, maxAge = 3600)
public class ProdottoController {
    @Autowired
    ProdottoRepository prodottoRepository;

    // @Autowired
    // ProdottiViewRepo  prodottiViewRepo;
    
    ProdottoService prodottoService;
    public ProdottoController(ProdottoService prodottoService){
      this.prodottoService=prodottoService;
    }
    
    @Operation(summary = "get list of users")
    @GetMapping("")
    public ResponseEntity<List<Prodotto>> list(){
      return new ResponseEntity<List<Prodotto>>(prodottoService.getAll(),HttpStatus.OK);
    }

    @GetMapping("/sottoscorta")
    public ResponseEntity<List<Prodotto>> findAllProdottiSottoScorta(){
      return new ResponseEntity<List<Prodotto>>(prodottoService.findAllProdottiSottoScorta(),HttpStatus.OK);
    }

    @GetMapping("/sottoscorta_view")
    public ResponseEntity<List<Prodotto>> findAllProdottiSottoScorta_fromView(){
      return new ResponseEntity<List<Prodotto>>(prodottoService.findAllProdottiSottoScorta_fromView(),HttpStatus.OK);
    }

    @GetMapping("/prodotti_view")
    public ResponseEntity<List<ProdottoDTO>> findAllProdottiView(){
      return new ResponseEntity<List<ProdottoDTO>>(prodottoRepository.findAllProdottiView(),HttpStatus.OK);
    }

    @GetMapping("/stessa_um/{um}/{giacenza}")
    public ResponseEntity<List<Prodotto>> findAllProdottiStessaUM(@PathVariable String um, double giacenza){
      return new ResponseEntity<List<Prodotto>>(prodottoService.findAllProdottiStessaUM(um,giacenza),HttpStatus.OK);
    }

    @GetMapping("/lista_id/{giacenza}")
    public ResponseEntity<List<IdProdottoDTO>> findByIdProdottoView(@PathVariable double giacenza){
      return new ResponseEntity<List<IdProdottoDTO>>(prodottoRepository.findByIdProdottoView(giacenza),HttpStatus.OK);
    }

    @PostMapping("")
    public ResponseEntity<Prodotto> add(@RequestBody Prodotto entity){
      if (prodottoService.save(entity).isPresent())
      return new ResponseEntity<Prodotto>(entity,HttpStatus.CREATED);
    else
      return new ResponseEntity<Prodotto>(HttpStatus.BAD_REQUEST);
 }

    @PutMapping("")
    public ResponseEntity<Prodotto>update(@RequestBody Prodotto entity){
      if (prodottoService.save(entity).isPresent())
        return new ResponseEntity<Prodotto>(entity,HttpStatus.CREATED);
      else
        return new ResponseEntity<Prodotto>(HttpStatus.BAD_REQUEST);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Prodotto>delete(@PathVariable Long id){
      if (prodottoService.delete(id).isPresent())
        return new ResponseEntity<Prodotto>(HttpStatus.OK);
      else
        return new ResponseEntity<Prodotto>(HttpStatus.NOT_FOUND);
    }

    @GetMapping("/valore")
    public ResponseEntity<List<ProdottoValoreDTO>> findAllProdottiValore(){
      return new ResponseEntity<List<ProdottoValoreDTO>>(prodottoRepository.findAllProdottiValore(),HttpStatus.OK);
    }
}
