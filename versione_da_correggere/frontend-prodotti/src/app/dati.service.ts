import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Prodotto } from './Prodotto';


export class DatiService {
  url : string =  "http://localhost:8080/api/prodotti"
  constructor(private http:HttpClient) { }

  getAll(): Observable<Prodotto[]>{
    return this.http.get<Prodotto[]>(this.url);
  }

  canc(id:number){
    return this.http.delete(this.url+"/"+id);
  }

  add(libro:Prodotto){
    return this.http.post(this.url,libro);
  }
  update(libro:Prodotto){
    return this.http.put(this.url,libro);
  }

}
