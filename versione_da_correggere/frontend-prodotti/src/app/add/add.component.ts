import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';

import { DatiService } from '../dati.service';
import { Prodotto } from '../Prodotto';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {
  @Input() prodotto:Prodotto= new Prodotto(); //this.iniLibro();
  @Output() fatto =new EventEmitter<boolean>();

  prodottofrm?:FormGroup;

  constructor(public datiService:DatiService, public fb: FormBuilder) { 
   
  }

  ngOnInit(): void {
    console.log(this.prodotto)
    this.prodottofrm= this.fb.group({
      id: [this.prodotto.id],
      'descrizione':[this.prodotto.descrizione],
      'giacenza':[this.prodotto.giacenza]      
    });
  }

  onSubmit(prodotto:Prodotto){
    if (this.prodotto.id!=0) // 
      this.datiService.update(prodotto).subscribe(res => 
        {
          this.fatto.emit(true);
        })
    else this.datiService.add(prodotto).subscribe(res => 
      {
        this.fatto.emit(true);
      })
  }

  annulla(){
    this.fatto.emit(false);
  }
}
