
import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { DatiService } from './dati.service';
import { Prodotto } from './Prodotto';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'crud';
  $prodotti : Observable<Prodotto[]>;
  isAdd:boolean=false;
  prodotto: Prodotto=new Prodotto();
  constructor(public datiService:DatiService){
    this.$prodotti=this.datiService.getAll()
  }
  add(){
    this.isAdd=! this.isAdd;
  }
  canc(id:number){
    this.datiService.canc(id).subscribe(res => {
      
      console.log(res);
      this.$prodotti=this.datiService.getAll()

    });
  }
  update(prodotto:Prodotto){
    this.prodotto=prodotto;
    this.add();
  }

  onFatto(fatto:boolean){
    this.add();
    this.prodotto=new Prodotto();
    if (fatto)
      this.$prodotti=this.datiService.getAll()
  }

}

