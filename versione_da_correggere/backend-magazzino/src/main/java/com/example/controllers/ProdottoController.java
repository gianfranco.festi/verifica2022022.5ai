package com.example.controllers;

import java.util.List;

import com.example.domains.Prodotto;
import com.example.services.ProdottoService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path="/api/prodotti", produces = "application/text")
@CrossOrigin(origins = "**", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT}, maxAge = 3600)
public class ProdottoController {

    

    ProdottoService prodottoService;
    public ProdottoController(ProdottoService prodottoService){
      this.prodottoService=prodottoService;
    }

    @GetMapping("")
    public ResponseEntity<List<Prodotto>> list(){
        return new ResponseEntity<List<Prodotto>>(prodottoService.getAll(),HttpStatus.BAD_REQUEST);
    }

    @PostMapping("")
    public ResponseEntity<Prodotto> add(@RequestBody Prodotto entity){
      if (prodottoService.save(entity).isPresent())
      return new ResponseEntity<Prodotto>(entity,HttpStatus.CREATED);
    else
      return new ResponseEntity<Prodotto>(HttpStatus.BAD_REQUEST);
 }

    @PutMapping("")
    public ResponseEntity<Prodotto>update(@RequestBody Prodotto entity){
      if (prodottoService.save(entity).isPresent())
        return new ResponseEntity<Prodotto>(entity,HttpStatus.CREATED);
      else
        return new ResponseEntity<Prodotto>(HttpStatus.BAD_REQUEST);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Prodotto>delete(@PathVariable Long id){
      if (prodottoService.delete(id).isPresent())
        return new ResponseEntity<Prodotto>(HttpStatus.OK);
      else
        return new ResponseEntity<Prodotto>(HttpStatus.NOT_FOUND);
    }
}
