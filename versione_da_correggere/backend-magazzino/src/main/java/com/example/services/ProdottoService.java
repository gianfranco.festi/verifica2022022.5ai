package com.example.services;

import java.util.List;
import java.util.Optional;


import com.example.domains.Prodotto;
import com.example.repositories.ProdottoRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
//@Transactional
public class ProdottoService {
    @Autowired
    ProdottoRepository prodottoRepository;
    public ProdottoService(){
        
    }

    public Optional<Prodotto> save(Prodotto entity){
        Prodotto prodotto=prodottoRepository.save(entity);
        Optional<Prodotto> opt=Optional.ofNullable(prodotto);
        return opt;
    }
    
    public Optional<Prodotto> update(Prodotto entity){
        Prodotto libro=prodottoRepository.save(entity);
        Optional<Prodotto> opt=Optional.ofNullable(libro);
        return opt;
    }
    public List<Prodotto> getAll(){
        return prodottoRepository.findAll();
    }
    public Optional<Prodotto> findById(Long id){
        return prodottoRepository.findById(id);
    }
    public Optional<Prodotto>delete(Long id){
        Optional<Prodotto> opt=prodottoRepository.findById(id);
        if (opt.isPresent()){
            prodottoRepository.deleteById(id);
        }
        return opt;
    }
    
}
