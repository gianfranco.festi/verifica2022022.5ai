insert into prodotti(descrizione,um,scorta_minima,giacenza,prezzo_unitario) values
    ("prodotto 1","kg"   ,10 ,100,2.5),
    ("prodotto 2","mc"   ,4  ,20 ,1.0),
    ("prodotto 3","litri",30 ,30 ,3.0),
    ("prodotto 4","kg"   ,2  ,20 ,4.0),
    ("prodotto 5","pezzi",1  ,6  ,7.8);
    